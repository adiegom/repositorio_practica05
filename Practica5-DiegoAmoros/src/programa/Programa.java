package programa;

import java.util.Scanner;

import clases.Clase1;
import clases.Clase2;
import clases.SuperClase;



/**
 * Clase Programa.
 * @author Hello there
 */
public class Programa {

	/**
	 * Metido main.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		
		int eleccion = 0;
		
		do {
			
			System.out.println("**************************************");
			System.out.println("�A quien quieres encontrar?");
			System.out.println("1. A alguien sensible a la fuerza sin faccion");
			System.out.println("2. A un Yedi");
			System.out.println("3. A un Sith");
			System.out.println("4. SALIR");
			System.out.println("**************************************");
			
			eleccion = input.nextInt();
			
			switch(eleccion) {
			
			case 1: //A alguien sensible a la fuerza sin faccion
			
				SuperClase sensibleALaFuerza = new SuperClase("Daris", "humano", 1, "amarillo", "Padawan", 0, "Shii-Cho");
				System.out.println("Nombre: " + sensibleALaFuerza.getNombre() + " ");
				System.out.println("Raza: " + sensibleALaFuerza.getRaza() + " ");
				System.out.println("Num Sables: " + sensibleALaFuerza.getSable() + " ");
				System.out.println("Color: " + sensibleALaFuerza.getColorSableLaser() + " ");
				System.out.println("Rango: " + sensibleALaFuerza.getRango() + " ");
				System.out.println("Entrenamiento: " + sensibleALaFuerza.getEntrenamiento() + " ");
				System.out.println("Estilo de Combate: " + sensibleALaFuerza.getEstiloCombate());
				
				System.out.println();
				System.out.println();
				
				sensibleALaFuerza.rankUp();
				System.out.println("Nombre: " + sensibleALaFuerza.getNombre() + " ");
				System.out.println("Raza: " + sensibleALaFuerza.getRaza() + " ");
				System.out.println("Num Sables: " + sensibleALaFuerza.getSable() + " ");
				System.out.println("Color: " + sensibleALaFuerza.getColorSableLaser() + " ");
				System.out.println("Rango: " + sensibleALaFuerza.getRango() + " ");
				System.out.println("Entrenamiento: " + sensibleALaFuerza.getEntrenamiento() + " ");
				System.out.println("Estilo de Combate: " + sensibleALaFuerza.getEstiloCombate());
				
				System.out.println();
				System.out.println();
				
				System.out.println("Ahora cambiaremos el estilo de combate");
				
				sensibleALaFuerza.ElegirEstiloDeCombate();
				System.out.println("Nombre: " + sensibleALaFuerza.getNombre() + " ");
				System.out.println("Raza: " + sensibleALaFuerza.getRaza() + " ");
				System.out.println("Num Sables: " + sensibleALaFuerza.getSable() + " ");
				System.out.println("Color: " + sensibleALaFuerza.getColorSableLaser() + " ");
				System.out.println("Rango: " + sensibleALaFuerza.getRango() + " ");
				System.out.println("Entrenamiento: " + sensibleALaFuerza.getEntrenamiento() + " ");
				System.out.println("Estilo de Combate: " + sensibleALaFuerza.getEstiloCombate());
				
				System.out.println();
				System.out.println();
				
				System.out.println("Ahora cambiaremos el equipamiento");
				
				sensibleALaFuerza.modificarEquipamiento();
				System.out.println("Nombre: " + sensibleALaFuerza.getNombre() + " ");
				System.out.println("Raza: " + sensibleALaFuerza.getRaza() + " ");
				System.out.println("Num Sables: " + sensibleALaFuerza.getSable() + " ");
				System.out.println("Color: " + sensibleALaFuerza.getColorSableLaser() + " ");
				System.out.println("Rango: " + sensibleALaFuerza.getRango() + " ");
				System.out.println("Entrenamiento: " + sensibleALaFuerza.getEntrenamiento() + " ");
				System.out.println("Estilo de Combate: " + sensibleALaFuerza.getEstiloCombate());
				
				System.out.println();
				System.out.println();
				
				
				break;
				//*************************************************
			case 2: // A un Yedi
				
				Clase1 jedi = new Clase1("Yoda", "Desconocida", 0, "verde", "Maestro Yedi", 18, "Ataru", "Yedi", 0, 0);
				System.out.println();
				System.out.print(jedi.getNombre() + " ");
				System.out.print(jedi.getRaza() + " ");
				System.out.print(jedi.getSable() + " ");
				System.out.print(jedi.getColorSableLaser() + " ");
				System.out.print(jedi.getRango() + " ");
				System.out.print(jedi.getEntrenamiento() + " ");
				System.out.print(jedi.getEstiloCombate() + " ");
				System.out.print(jedi.getFaccion() + " ");
				System.out.print(jedi.getNivelOdio() + " ");
				System.out.print(jedi.getMisionFallida());
				
				System.out.println();
				System.out.println();
				
				jedi.MisionesFallidas();
				
				input.nextLine();
				
				jedi.corrupcion();
				
				
				jedi.nSablesPlus();
				System.out.println("Nombre: " + jedi.getNombre() + " ");
				System.out.println("Raza: " + jedi.getRaza() + " ");
				System.out.println("Num Sables: " + jedi.getSable() + " ");
				System.out.println("Color: " + jedi.getColorSableLaser() + " ");
				System.out.println("Rango: " + jedi.getRango() + " ");
				System.out.println("Entrenamiento: " + jedi.getEntrenamiento() + " ");
				System.out.println("Estilo Combate: " + jedi.getEstiloCombate() + " ");
				System.out.println("Faccion: " + jedi.getFaccion() + " ");
				System.out.println("Nivel de Odio: " + jedi.getNivelOdio() + " ");
				System.out.println("Misiones fallidas: " + jedi.getMisionFallida());
				System.out.println();
				System.out.println();
				
				break;
				//*************************************************
			case 3: //A un Sith
				
				Clase2 sith = new Clase2("Jar Jar Binks", "Gungan", 1, "rojo", "Lord Sith", 100, "Juyo / Vaapad",
						"Sith", "Rep�blica Gal�ctica", 0);
				System.out.println();
				System.out.println("Nombre: " + sith.getNombre() + " ");
				System.out.println("Raza: " + sith.getRaza() + " ");
				System.out.println("Num Sables: " + sith.getSable() + " ");
				System.out.println("Color: " + sith.getColorSableLaser() + " ");
				System.out.println("Rango: " + sith.getRango() + " ");
				System.out.println("Entrenamiento: " + sith.getEntrenamiento() + " ");
				System.out.println("Estilo de Combate: " + sith.getEstiloCombate() + " ");
				System.out.println("Faccion: " + sith.getFaccion() + " ");
				System.out.println("Era: " + sith.getEra() + " ");
				System.out.println("Misiones Falladas: " + sith.getFallosMision());
				
				System.out.println();
				System.out.println();
				
				sith.rankUp();
				System.out.print(sith.getRango() + " ");
				System.out.println();
				
				sith.degradacion();
				System.out.print(sith.getRango() + " ");
				System.out.println();
				
				sith.corromperCristalKyber();
				System.out.println("Nombre: " + sith.getNombre() + " ");
				System.out.println("Raza: " + sith.getRaza() + " ");
				System.out.println("Num Sables: " + sith.getSable() + " ");
				System.out.println("Color: " + sith.getColorSableLaser() + " ");
				System.out.println("Rango: " + sith.getRango() + " ");
				System.out.println("Entrenamiento: " + sith.getEntrenamiento() + " ");
				System.out.println("Estilo de Combate: " + sith.getEstiloCombate() + " ");
				System.out.println("Faccion: " + sith.getFaccion() + " ");
				System.out.println("Era: " + sith.getEra() + " ");
				System.out.println("Misiones Falladas: " + sith.getFallosMision());
				
				//*************************************************
			case 4: // SALIR
				break;
			default:
				System.out.println("Elija otra ocion que sea correcta");
				break;
			
			}
			
			
		} while(eleccion >= 1 && eleccion < 4);
		
	}

}
