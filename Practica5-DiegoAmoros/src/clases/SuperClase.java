package clases;

import java.util.Scanner;


// TODO: Auto-generated Javadoc
/**
 * SuperClase.
 *  Esta clase es la que alberga los setters y getters de la
 *  superclase.
 * @author Hello there
 * 
 * 
 * 
 */
public class SuperClase {

	/** nombre. */
	protected String nombre;
	
	/** raza. */
	protected String raza;
	
	/** sable. */
	protected int sable;
	
	/** color sable laser. */
	protected String colorSableLaser;
	
	/** rango. */
	protected String rango;
	
	/** entrenamiento. */
	protected double entrenamiento;
	
	/** Estilo combate. */
	protected String EstiloCombate;

	/**
	 * Instanciacion super clase.
	 */
	public SuperClase() {

		this.nombre = "-";
		this.raza = "-";
		this.sable = 0;
		this.colorSableLaser = "-";
		this.rango = "-";
		this.entrenamiento = 0;
		this.EstiloCombate = "-";
	}

	/**
	 * Instanciacion super clase.
	 *
	 * @param nombre the nombre
	 * @param raza the raza
	 * @param sable the sable
	 * @param colorSableLaser the color sable laser
	 * @param rango the rango
	 * @param entrenamiento the entrenamiento
	 * @param EstiloCombate the estilo combate
	 */
	public SuperClase(String nombre, String raza, int sable, String colorSableLaser, String rango, double entrenamiento,
			String EstiloCombate) {

		this.nombre = nombre;
		this.raza = raza;
		this.sable = sable;
		this.colorSableLaser = colorSableLaser;
		this.rango = rango;
		this.entrenamiento = entrenamiento;
		this.EstiloCombate = EstiloCombate;
	}

	
	/**
	 * Gets the nombre.
	 *
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Sets the nombre.
	 *
	 * @param nombre the new nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
	/**
	 * Gets the raza.
	 *
	 * @return the raza
	 */
	public String getRaza() {
		return raza;
	}


	/**
	 * Sets the raza.
	 *
	 * @param raza the new raza
	 */
	public void setRaza(String raza) {
		this.raza = raza;
	}


	/**
	 * Gets the sable.
	 *
	 * @return the sable
	 */
	public int getSable() {
		return sable;
	}


	/**
	 * Sets the sable.
	 *
	 * @param sable the new sable
	 */
	public void setSable(int sable) {
		this.sable = sable;
	}


	/**
	 * Gets the color sable laser.
	 *
	 * @return the color sable laser
	 */
	public String getColorSableLaser() {
		return colorSableLaser;
	}


	/**
	 * Sets the color sable laser.
	 *
	 * @param colorSableLaser the new color sable laser
	 */
	public void setColorSableLaser(String colorSableLaser) {
		this.colorSableLaser = colorSableLaser;
	}


	/**
	 * Gets the rango.
	 *
	 * @return the rango
	 */
	public String getRango() {
		return rango;
	}


	/**
	 * Sets the rango.
	 *
	 * @param rango the new rango
	 */
	public void setRango(String rango) {
		this.rango = rango;
	}


	/**
	 * Gets the entrenamiento.
	 *
	 * @return the entrenamiento
	 */
	public double getEntrenamiento() {
		return entrenamiento;
	}


	/**
	 * Sets the entrenamiento.
	 *
	 * @param entrenamiento the new entrenamiento
	 */
	public void setEntrenamiento(double entrenamiento) {
		this.entrenamiento = entrenamiento;
	}


	/**
	 * Gets the estilo combate.
	 *
	 * @return the estilo combate
	 */
	public String getEstiloCombate() {
		return EstiloCombate;
	}


	/**
	 * Sets the estilo combate.
	 *
	 * @param estiloCombate the new estilo combate
	 */
	public void setEstiloCombate(String estiloCombate) {
		EstiloCombate = estiloCombate;
	}


	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "SuperClase [nombre=" + nombre + ", raza=" + raza + ", sable=" + sable + ", colorSableLaser="
				+ colorSableLaser + ", rango=" + rango + "]";
	}

	/**
	 * clase que sirve para subir de nivel, para aumentar el rango.
	 */
	public void rankUp() {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce la experiencia que ha ganado");
		int entrenamiento = input.nextInt(); 
		
		if (entrenamiento <= 24) {
			rango = "Padawan";

			System.out.println("¡¡Te falta mucho camino joven Padawan!! Sigue esforzandote.");

		}

		if (entrenamiento >= 25 && entrenamiento <= 49) {
			rango = "Caballero Yedi";

			System.out.println("¡¡Enhorabuena, has ascendido a " + rango + " !! Sigue esforzandote.");

		}

		if (entrenamiento >= 50 && entrenamiento <= 74) {
			rango = "Consul Yedi";

			System.out.println("¡¡Enhorabuena, has ascendido a " + rango + " !! Esperamos mucho de ti.");

		}

		if (entrenamiento >= 75 && entrenamiento < 100) {
			rango = "Maestro Yedi";

			System.out.println("¡¡Enhorabuena, has ascendido a " + rango
					+ " !! Te encuentras en la elite jedi, sientete orgulloso.");

		}
		

	}

	/**
	 * clase para poder cambiar el estilo de combate.
	 */
	public void ElegirEstiloDeCombate() {
		Scanner input = new Scanner(System.in);
		boolean eleccion = true;
		while (eleccion) {

			System.out.println("**************************************");
			System.out.println("¿En que estilo te quieres centrar?");
			System.out.println("1. Forma I: Shii-Cho");
			System.out.println("2. Forma II: Makashi");
			System.out.println("3. Forma III: Soresu");
			System.out.println("4. Forma IV: Ataru");
			System.out.println("5. Forma V: Shien / Djem So");
			System.out.println("6. Forma VI: Niman");
			System.out.println("7. Forma VII: Juyo / Vaapad");
			System.out.println("8. SALIR");
			System.out.println("**************************************");

			int menu = 0;

			while (eleccion) {
				String dato = input.nextLine();
				if (dato.length() != 1) {
					System.out.println("Introduzca una opcion entre las posibles");
					eleccion = true;
				} else {
					if (dato.charAt(0) >= '0' && dato.charAt(0) <= '8') {
						eleccion = false;
						menu = Integer.parseInt(dato);
					} else {
						System.out.println("Introduzca una opcion entre las posibles");
						eleccion = true;
					}
				}
			}

			switch (menu) {

			case 1:
				this.EstiloCombate = "Shii-Cho";
				eleccion = false;
				break;

			case 2:
				this.EstiloCombate = "Makashi";
				eleccion = false;
				break;

			case 3:
				this.EstiloCombate = "Soresu";
				eleccion = false;
				break;

			case 4:
				this.EstiloCombate = "Ataru";
				eleccion = false;
				break;

			case 5:
				this.EstiloCombate = "Shien / Djem So";
				eleccion = false;
				break;

			case 6:
				this.EstiloCombate = "Niman";
				eleccion = false;
				break;

			case 7:
				this.EstiloCombate = "Juyo / Vaapad";
				eleccion = false;
				break;

			case 8:
				break;

			default:
				System.out.println("Introduzca una opcion entre las posibles");
				eleccion = true;
			}
		}
		

	}

	/**
	 * Modificar equipamiento.
	 */
	public void modificarEquipamiento() {

		Scanner input = new Scanner(System.in);
		boolean eleccion = true;
		while (eleccion) {

			System.out.println("**************************************");
			System.out.println("¿Que quieres equiparte?");
			System.out.println("1. Sable laser unico");
			System.out.println("2. Dos sables laser");
			System.out.println("3. Sable laser doble");
			System.out.println("4. Tres sables laser");
			System.out.println("5. Cuatro sables laser");
			System.out.println("6. SALIR");
			System.out.println("**************************************");

			int menu = 0;

			while (eleccion) {
				String dato = input.nextLine();
				if (dato.length() != 1) {
					System.out.println("Introduzca una opcion entre las posibles");
					eleccion = true;
				} else {
					if (dato.charAt(0) >= '0' && dato.charAt(0) <= '5') {
						eleccion = false;
						menu = Integer.parseInt(dato);
					} else {
						System.out.println("Introduzca una opcion entre las posibles");
						eleccion = true;
					}
				}
			}

			switch (menu) {

			case 1:
				this.sable = 1;
				eleccion = false;
				break;

			case 2:
				this.sable = 2;
				eleccion = false;
				break;

			case 3:
				this.sable = 1;
				eleccion = false;
				break;

			case 4:
				this.sable = 3;
				eleccion = false;
				break;

			case 5:
				this.sable = 4;
				eleccion = false;
				break;

			case 6:
				break;

			default:
				System.out.println("Introduzca una opcion entre las posibles");
				eleccion = true;
			}
		}
		

	}

}
