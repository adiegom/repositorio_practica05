package clases;

import java.util.Scanner;



/**
 * Clase2.
 */
public class Clase2 extends SuperClase {

	/** faccion. */
	private String faccion = "Sith";
	
	/** era. */
	private String era;
	
	/** fallos mision. */
	private int fallosMision;

	/**
	 * Instanciacion sub clase 2.
	 */
	public Clase2() {

		super();
		this.faccion = "-";
		this.era = "-";
		this.fallosMision = 0;

	}

	
	/**
	 * Instantiates a new clase 2.
	 *
	 * @param nombre. Te permite a�adir un nombre.
	 * @param raza. Te permite a�adir un raza.
	 * @param sable. Te permite a�adir un sable.
	 * @param colorSableLaser. Te permite a�adir un color.
	 * @param rango. Te permite a�adir un rango.
	 * @param entrenamiento. Te permite a�adir un nivel de entrenamiento.
	 * @param EstiloCombate. Te permite a�adir un estilo de combate.
	 * @param faccion. Te permite a�adir una faccion
	 * @param era. Te permite a�adir una era temporal. 
	 * @param fallosMision. Te permite a�adir un fallos.
	 */
	public Clase2(String nombre, String raza, int sable, String colorSableLaser, String rango, double entrenamiento,
			String EstiloCombate,String faccion, String era, int fallosMision) {

		super(nombre, raza, sable, colorSableLaser, rango, entrenamiento, EstiloCombate);
		this.faccion = "Sith";
		this.era = era;
		this.fallosMision = fallosMision;
		this.sable = 1;

	}

	
	/**
	 * Gets the faccion.
	 *
	 * @return the faccion
	 */
	public String getFaccion() {
		return faccion;
	}

	
	/**
	 * Sets the faccion.
	 *
	 * @param faccion the new faccion
	 */
	public void setFaccion(String faccion) {
		this.faccion = faccion;
	}

	
	/**
	 * Gets the era.
	 *
	 * @return the era
	 */
	public String getEra() {
		return era;
	}

	
	/**
	 * Sets the era.
	 *
	 * @param era the new era
	 */
	public void setEra(String era) {
		this.era = era;
	}

	
	/**
	 * Gets the fallos mision.
	 *
	 * @return the fallos mision
	 */
	public int getFallosMision() {
		return fallosMision;
	}

	
	/**
	 * Sets the fallos mision.
	 *
	 * @param fallosMision the new fallos mision
	 */
	public void setFallosMision(int fallosMision) {
		this.fallosMision = fallosMision;
	}

	
	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Clase2 [faccion=" + faccion + ", era=" + era + ", fallosMision=" + fallosMision + "]";
	}

	
	/**
	 * Rank up.
	 * poco mas que decir, subes nivel
	 */
	public void rankUp() {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce la experiencia que ha ganado");
		int entrenamiento = input.nextInt(); 

		if (entrenamiento <= 24 && rango == null || rango == "Aprendiz") {
			rango = "Aprendiz";

			System.out.println("��Te falta mucho camino !! Sigues siendo debil, te falta odio.");

		}

		if (entrenamiento >= 25 && entrenamiento <= 49 && rango == "Aprendiz") {
			rango = "Maestro Sith";

			System.out.println("��Enhorabuena, has ascendido a " + rango
					+ " !! Sigue sintiendo odio, pues es lo que te hara poderoso.");

		}

		if (entrenamiento >= 50 && entrenamiento <= 74 && rango == "Maestro Sith") {
			rango = "Lord Sith";

			System.out.println("��Enhorabuena, has ascendido a " + rango
					+ " !! Esperamos mucho de ti, tu poder es inconmensurable.");

		}

		if (entrenamiento >= 75 && entrenamiento <= 100 && rango == "Lord Sith") {
			rango = "Se�or oscuro Sith";

			System.out.println("��Ahora tu eres el mas poderoso Sith !! Ahora tu das las ordenes `Maestro`.");

		}

	}

	/**
	 * Degradacion.
	 * Cambia el rango a menos
	 */
	public void degradacion() {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce las misiones que ha fallado (de 0 a 5)");
		int fallosMision = input.nextInt(); 

		if (rango.equalsIgnoreCase("Aprendiz") && fallosMision >= 5) {
			rango = "Despojo";

			System.out.println("Debido a tu incompetencia eres sentenciado a muerte. ��EJECUTADLO!!");						

		}

		if (rango.equalsIgnoreCase("Maestro Sith") && fallosMision >= 5) {
			rango = "Aprendiz";

			System.out.println("��Debido a tu incompetencia, has sido degradado a " + rango
					+ " !! Me das asco, largo de mi vista");

		}

		if (rango.equalsIgnoreCase("Lord Sith") && fallosMision >= 5) {
			rango = "Maestro Sith";

			System.out.println("��Debido a tu incompetencia, has sido degradado a " + rango
					+ " !! Penoso, que no vuelva a ocurrir");

		}
	}

	/**
	 * Corromper cristal kyber.
	 * Obtienes sable rojo sith
	 */
	public void corromperCristalKyber() {
		if (faccion == "Sith" && !colorSableLaser.equalsIgnoreCase("rojo")) {

			colorSableLaser = "Rojo";

		}
	}

}
