package clases;

import java.util.Scanner;


/**
 * Clase1.
 * Primera subclase
 */
public class Clase1 extends SuperClase {

	/** Escaner. */
	static Scanner input = new Scanner(System.in);

	/** faccion. */
	private String faccion = "Yedi";
	
	/** nivel odio. */
	private int nivelOdio;
	
	/** mision fallida. */
	private int misionFallida;

	/** pase. */
	static int pase;

	/**
	 * Instanciacion sub clase 1.
	 */
	public Clase1() {
		super();
		this.faccion = "-";
		this.nivelOdio = 0;
		this.misionFallida = 0;
	}

	
	/**
	 * Instantiates a new clase 1.
	 *
	 * @param nombre. Te permite a�adir un nombre.
	 * @param raza. Te permite a�adir una raza.
	 * @param sable. Te permite a�adir un sable.
	 * @param colorSableLaser es el color del sable laser. Te permite a�adir un color.
	 * @param rango. Te permite a�adir un rango.
	 * @param entrenamiento. Te permite a�adir un entrenamiento.
	 * @param EstiloCombate. Te permite a�adir un estilo de combate.
	 * @param faccion. Te permite a�adir una faccion.
	 * @param nivelOdio. Te permite a�adir un nivel de odio.
	 * @param misionFallida. Te permite a�adir la cantidad de misiones fallidas.
	 */
	public Clase1(String nombre, String raza, int sable, String colorSableLaser, String rango, double entrenamiento,
			String EstiloCombate, String faccion, int nivelOdio, int misionFallida) {
		super(nombre, raza, sable, colorSableLaser, rango, entrenamiento, EstiloCombate);
		this.faccion = faccion;
		this.nivelOdio = nivelOdio;
		this.misionFallida = misionFallida;
	}

	
	/**
	 * Gets the faccion.
	 *
	 * @return the faccion
	 */
	public String getFaccion() {
		return faccion;
	}

	
	/**
	 * Sets the faccion.
	 *
	 * @param faccion the new faccion
	 */
	public void setFaccion(String faccion) {
		this.faccion = faccion;
	}


	/**
	 * Gets the nivel odio.
	 *
	 * @return the nivel odio
	 */
	public int getNivelOdio() {
		return nivelOdio;
	}

	
	/**
	 * Sets the nivel odio.
	 *
	 * @param nivelOdio the new nivel odio
	 */
	public void setNivelOdio(int nivelOdio) {
		this.nivelOdio = nivelOdio;
	}

	
	/**
	 * Gets the mision fallida.
	 *
	 * @return the mision fallida
	 */
	public int getMisionFallida() {
		return misionFallida;
	}


	/**
	 * Sets the mision fallida.
	 *
	 * @param misionFallida the new mision fallida
	 */
	public void setMisionFallida(int misionFallida) {
		this.misionFallida = misionFallida;
	}


	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Clase1 [faccion=" + faccion + ", nivelOdio=" + nivelOdio + ", misionFallida=" + misionFallida + "]";
	}

	/**
	 * Misiones fallidas.
	 * Sirve para aumentar el nivel de odio de un jedi.
	 */
	public void MisionesFallidas() {

		Scanner input = new Scanner(System.in);

		System.out.println("**************************************");
		System.out.println("�Cuantas misiones ha fallado?");
		System.out.println("**************************************");

		int misionFallida = input.nextInt();

		for (int i = 0; i <= misionFallida; i++)
			nivelOdio = nivelOdio + 3;

	}

	/**
	 * Corrupcion.
	 * Sirve para disminuir el rango.
	 */
	public void corrupcion() {

		if (this.nivelOdio >= 50) {

			faccion = "Sith";
			rango = "Aprendiz";

		}
	}

	/**
	 * N sables plus.
	 * Otorga el primer sable
	 */
	public void nSablesPlus() {
		if (sable != 0) {
			System.out.println("Ya tienes sable de luz");

		} else {

			System.out.println("Estas seguro que quieres obtener un sable? s/n");
			String respuesta = input.nextLine();
			if (respuesta == "s") {
				sable = sable + 1;
			}

		}
	}

}
